SHIFU
Behold. The Dragon Scroll... It is yours.

[Po reaches for the scroll, but hesitates.]

PO
Wait... what happens when I read it?

SHIFU
No one knows, but legend says you will be able to hear a butterfly's wing-beat.

PO
Whoa! Really? That's cool.

SHIFU
Yes. And see light in the deepest cave. You will feel the universe in motion around you.

PO
[Gets overexcited.] Wow! Can I punch through walls? Can I do a quadruple back flip? Will I have invisibility—

SHIFU
Focus... focus.

PO
Huh? Oh, yeah... yeah.

SHIFU
Read it, Po, and fulfill your destiny. Read it and become... the Dragon Warrior!

PO
Whooaa!!!

[Po takes the scroll. Then he grasps the tube and tries to pull the top off it. It doesn't budge. He strains at it.]

PO
It's impossible to open.

[He strains again. He tries to bite it off...]

PO
Come on, baby. Come on now...

[Shifu holds out his paw. Po passes him the scroll. Shifu pops the end off effortlessly and passes it back to Po.]

PO
Thank you. I probably loosened it up for you though... Okay, here goes.

[Po starts to unroll the scroll, the golden light bathing his face. Across the scroll we see Shifu, excited that he is witness to history... On Po's face as he finishes opening the scroll... then...]

PO
AAAAAAAAAAAAAAAAAHHH!!!

[Shifu and the five are startled. Po looks utterly terrified.]

PO
It's blank!

SHIFU
What!?

PO
Here! Look!

[Po tries to show Shifu the scroll. Shifu covers his eyes and turns his head away.]

SHIFU
No! I am forbidden to look upon...

[But he can't help himself. He takes a peek. Then he grabs it off Po. He turns it around, then upside down. He closes it and opens it again, astonished.]

SHIFU
Blank? I don't...I don't understand.
